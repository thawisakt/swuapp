<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en"  xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>E-FORM-TUTOR</title>
</head>
<body>
<div class="jumbotron">
  <h1>ระบบรับสมัครสอนพิเศษ</h1> 
  <p>งานบริการจัดหางาน งานแนะแนวให้คำปรึกษา</p> 
</div>
    <div  class ="container">
  
        <form>
         <div class="form-group">
           <div class="table">
               
               <fieldset  class="form-group"><legend>ประวัติส่วนตัว</legend>
               <div class ="row">
                   <div class = "col-sm-2">
                       <label>ชื่อ :</label>
                    </div>
                    <div class = "col-sm-2">
                        <select class ="form-control" id ="prefix">
                            <option value = "">เลือก</option>
                            <option value ="1">นาย</option>
                            <option value ="2">นาง</option>
                            <option value ="3">นางสาว</option>
                        </select>
                    </div>
                    <div class ="col-sm-4">
                        <input type="text" name ="fname" id="fname" placeholder ="กรุณาใส่ชื่อ" class ="form-control">
                    </div>
                    <div class ="col-sm-4">
                        <input type="text" name ="lname" id="lname" placeholder ="กรุณาใส่นามสกุล" class ="form-control">
                    </div>
                </div>
                <div class ="row">
                    <div class ="col-sm-4" >
                        <label>ที่อยู่ปัจจุบันที่สามารถติดต่อได้  บ้านเลขที่ :</label>
                    </div>
                    <div class ="col-sm-2">
                    <input type="text" name ="address" id="address" placeholder ="กรุณาใส่บ้านเลขที่" class ="form-control">
                    </div>
                    <div class ="col-sm-2" >
                        <label>ตรอกซอย :</label>
                    </div>
                    <div class ="col-sm-4">
                    <input type="text" name ="soi" id="soi" placeholder ="กรุณาใส่ตรอกซอย" class ="form-control">
                    </div>
                </div>

                <div class ="row">
                    <div class ="col-sm-2"  >
                        <label>ถนน :</label>
                    </div>
                    <div class ="col-sm-4">
                    <input type="text" name ="street" id="street" placeholder ="กรุณาใส่ถนน" class ="form-control">
                    </div>
                    <div class ="col-sm-2"  >
                        <label>จังหวัด :</label>
                    </div>
                    <div class ="col-sm-4">
                     <select id="province" class= "form-control">
                        <option>- กรุณาเลือกจังหวัด -</option>
                        </select>
                    </div>
                </div> 
                <div class ="row">
                    <div class ="col-sm-2" >
                        <label>อำเภอ :</label>
                    </div>
                    <div class ="col-sm-4">
                    <select id="amphur" class= "form-control">
                        <option>- กรุณาเลือกอำเภอ -</option>
                        </select>
                    </div>
                    <div class ="col-sm-2" >
                        <label>ตำบล :</label>
                    </div>
                    <div class ="col-sm-4">
                    <select id="district" class= "form-control">
                        <option>- กรุณาเลือกตำบล -</option>
                        </select>
                    </div>
                </div>
                <div class ="row">
                    <div class ="col-sm-2">
                    <label>รหัสไปรษณีย์ :</label>
                </div>
                <div class ="col-sm-4">
                    <input type = "text" name ="postcode" id="postcode" class ="form-control">
                </div>
                 <div class ="col-sm-2" >
                     <label>โทรศัพท์ :</label>
                 </div>
                 <div class ="col-sm-4">
                    <input type = "text" name ="tel" id="tel" class ="form-control">
                </div>
                </div>
                <div class ="row">
                    <div class ="col-sm-2" >
                        <label>E-mail :</label>
                    </div>
                    <div class ="col-sm-4">
                        <input type ="email" name ="email" id ="email" class ="form-control">
                    </div>
                </div>
            </fieldset>
           </div>  
</div>  
       <div class ="form-group">
       <fieldset><legend>ระดับชั้นที่สามารถสอนได้</legend>
           <div class="row">
               <div class ="col-sm-2"></div>
               <div class="col-sm-3" align ="left">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        อนุบาล
                    </label></div>
                    <div class="col-sm-3" align ="left">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                    <label class="form-check-label" for="defaultCheck2">
                    ป.1 - ป.6 
                    </label>
               </div>
               <div class="col-sm-3" align ="left">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
                    <label class="form-check-label" for="defaultCheck3">
                    ม.1 - ม.3
                    </label>
               </div>
               </div>
               <div class="row">
               <div class = "col-sm-2"></div>
               <div class="col-sm-3" align ="left">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck4">
                    <label class="form-check-label" for="defaultCheck4">
                    ม.4 - ม.6
                    </label></div>
                    <div class="col-sm-3" align ="left">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck5">
                    <label class="form-check-label" for="defaultCheck5">
                    ปวช. - ปวส. 
                    </label>
               </div>
               <div class="col-sm-3" align ="left">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck6">
                    <label class="form-check-label" for="defaultCheck6">
                    ปริญญาตรี
                    </label>
               </div>
               <div class = "col-sm-1"></div>
               </div>
</fieldset>
</div>

        <div class ="form-group">
            <fieldset><legend>ประสบการณ์ด้านการสอน</legend>
            <div class ="row">
                <div class ="col-sm-2"></div>
                <div class ="col-sm-1">
                   <input class="form-check-input" type="checkbox" value="" id="defaultCheck7">
                    <label class="form-check-label" for="defaultCheck7">
                    มี
                    </label>
                    </div>
                    <div class ="col-sm-2">
                    <input type="text" name ="year" id ="year" class ="form-control" >
                </div>
                <div class ="col-sm-1" align ="right">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck8">
                    <label class="form-check-label" for="defaultCheck8">
                    ไม่มี
                    </label>
                </div>
            </div>
            <div class ="row">
                <div class = "col-sm-2"></div>
                <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck9">
                    <label class="form-check-label" for="defaultCheck9">
                    คณิตศาสตร์
                    </label>
                </div>
                <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck10">
                    <label class="form-check-label" for="defaultCheck10">
                    วิทยาศาสตร์
                    </label>
                </div>
                <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck11">
                    <label class="form-check-label" for="defaultCheck11">
                    เคมี
                    </label>
                </div>
                <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck12">
                    <label class="form-check-label" for="defaultCheck12">
                    ชีววิทยา
                    </label>
                </div>
                </div>
            <div class ="row">
                <div class = "col-sm-2"></div>
                <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck13">
                    <label class="form-check-label" for="defaultCheck13">
                    ภาษาอังกฤษ
                    </label>
                </div>
                <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck14">
                    <label class="form-check-label" for="defaultCheck14">
                    ฟิสิกส์
                    </label>
                </div>
                <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck15">
                    <label class="form-check-label" for="defaultCheck15">
                    ภาษาไทย
                    </label>
                </div>
                <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck16">
                    <label class="form-check-label" for="defaultCheck16">
                    สปช
                    </label>
                </div>
            </div>
            <div class ="row">
            <div class ="col-sm-2"></div>
            <div class ="col-sm-2">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck17">
                    <label class="form-check-label" for="defaultCheck17">
                    ทุกวิชา
                    </label>
                </div>
                <div class ="col-sm-1">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck18">
                    <label class="form-check-label" for="defaultCheck18">
                    อื่นๆ
                    </label>
                </div>
                <div class ="col-sm-4">
                    <input type ="text" class ="form-control" name ="other" id ="other">
                    </div>

            </div>
        </fieldset>
        </div>
        <div class = "form-group">
        <fieldset><legend>ประสบการณ์ด้านการสอน</legend>
            <div class ="row">
                <div class ="col-sm-2">
                    <label>ปัจจุบันเรียนอยู่ชั้นปีที่</label>
                </div>
                <div class ="col-sm-4">
                <select class = "form-control" id = "LevelClass">
				    <option value="">กรุณาเลือก</option> 
				    <option value="1">1</option> 
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>	
		       </select>
                </div>
                <div class ="col-sm-2">
                    <label>วิชาเอก</label>
                </div>
                <div class ="col-sm-4">
                     <input type ="text" name ="main" id = "main" class ="form-control">
                </div>
            </div>
            <div class = "row">
                <div class = "col-sm-2">
                    <label>วิชาโท</label>
                </div>
                <div class ="col-sm-4">
                <input type ="text" name ="subMain" id = "subMain" class= "form-control">
                </div>
                <div class = "col-sm-2">
                    <label>คณะ</label>
                </div>
                <div class ="col-sm-4">
                <input type ="text" name ="department" id = "department" class ="form-control">
                </div>
            </div>
            <div class = "row">
                <div class = "col-sm-2">
                    <label>รหัสนิสิต</label>
                </div>
                <div class ="col-sm-4">
                <input type ="text" name ="std_id" id = "std_id" class= "form-control">
                </div>
                <div class = "col-sm-2">
                    <label>อาจารย์ที่ปรึกษา</label>
                </div>
                <div class ="col-sm-4">
                <input type ="text" name ="advisors" id = "advisors" class ="form-control">
                </div>
            </div>

            <div class = "row">
                <div class = "col-sm-2">
                    <label>เกรดเฉลี่ย</label>
                </div>
                <div class ="col-sm-4">
                <input type ="text" name ="gpa" id = "gpa" class= "form-control">
                </div>
                <div class = "col-sm-3">
                    <label>จบการศึกษาระดับชั้นมัธยมศึกษาปีที่</label>
                </div>
                <div class ="col-sm-3">
                <input type ="text" name ="highSchool" id = "highSchool" class ="form-control">
                </div>
            </div>
            <div class = "row">
                <div class = "col-sm-2">
                    <label>โรงเรียน</label>
                </div>
                <div class ="col-sm-4">
                <input type ="text" name ="school" id = "school" class= "form-control">
                </div>
                <div class = "col-sm-3">
                    <label>วันเวลาที่นิสิตสามารถปฏิบัติงานได้(วันเวลาที่นิสิตไม่มีเรียน) </label>
                </div>
                <div class ="col-sm-3">
                <input type ="date" name ="date_job" id = "date_job" class= "form-control">
                </div>
            </div>
            </fieldset>
            <div class ="row">
                <div class ="col-sm-12" align ="center">
                    <button type = "button" id ="save" class ="btn btn-success">SAVE</button>
                    <button type = "button" id ="clear" class ="btn btn-secondary">CLEAR</button>
                </div>
                
            </div>
        </div>



</form>

</div>

<div class="card-footer text-muted">
    Footer
  </div>






<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="AutoProvince.js"></script>
<script>
	$('body').AutoProvince({
       
        
		PROVINCE:		'#province', // select div สำหรับรายชื่อจังหวัด
        AMPHUR:			'#amphur', // select div สำหรับรายชื่ออำเภอ
        DISTRICT:		'#district', // select div สำหรับรายชื่อตำบล
		POSTCODE:		'#postcode', // input field สำหรับรายชื่อรหัสไปรษณีย์
		arrangeByName:		false // กำหนดให้เรียงตามตัวอักษร
	});
</script>




<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>






  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  
</body>
</html>