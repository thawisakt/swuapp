<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!-- viewport : เว็บไซต์แบบ responsive-->
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        d{
            color: red;
        }
    </style>
    <title>Document</title>
</head></br>
<body>
    <div class="container">
    <form  method = "POST" id="frmForm" >
    <div class="row">
    <div class="col-sm-1">
    ชื่อ<d>*</d> </div>
    <div class ="col-sm-5"> <input type="text" class="form-control" id="firstname" name="firstname" placeholder ="กรุณาใส่ชื่อ" data-title="first name"  >
    <div class="alert alert-light"  role="alert" id ="afname" style="display:none;">
  <d>Please input your firstname!</d>
</div></div>
    <div class="col-sm-1">
    นามสกุล<d>*</d> </div>
    <div class="col-sm-5"><input type="text" class="form-control" id ="lastname" name="lastname" placeholder ="กรุณาใส่นามสกุล" data-title="last name" > <!--placeholder : ใส่ตัวบางๆที่ ช่องกรอก -->
 
    <div class="alert alert-light" role="alert" id="alname" style="display:none;">
    <d>Please input your lastname!</d>
</div>  </div>
  </div></br>
  <div class="row">
    <div class="col-sm-1">
    ตำแหน่ง<d>*</d></div>
    <div class ="col-sm-5">
      <select class="form-control" id= "position"  data-title="position"  >
        <option value="">กรุณาเลือก</option>
        <option value="Developer">Developer</option>
        <option value="SA">SA</option>
        <option value="PM">PM</option>
        </select> 
        <div class="alert alert-light" role="alert" id ="aposition" style="display:none;">
        <d>Please select your position!</d>
    </div></div> </div> </br>
    <div class="row">
    <div class="col-sm-12" align ="center">
    <button type ="submit" id ="save" class="btn btn-success">Save</button>
   <!-- <input type="text" id="result">   สร้างtext มารับค่าเมื่อกด view  จาก script response -->
    <button type ="button" id ="clear" class="btn btn-secondary">Clear</button>
  <!--  <button type ="button" id ="clear">Clear</button> -->
    </div> 
    </form>

</div>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<script>
    
$(function () {
    $('#clear').click(function (e) { 
        e.preventDefault();

        $('input').val("");
        $('select').val("");
        $('#firstname').focus();
        $('input').removeAttr('disabled');
        $('select').removeAttr('disabled');
        $('#save').removeAttr('disabled'); 
        $('.alert').hide();
    
    });

    $('form').submit(function (e) { 
        e.preventDefault();
        if(!validate()){ 
               if(!validate_hide()){
                   if(!checkLen()){
                        $('input').attr('disabled', true);
                        $('select').attr('disabled', true);
                        $('#save').attr('disabled', true);
                   }
               }
              // return false;
        }
        return false;
    }); 
})


function  checkLen() {
    var strFname = $('#firstname').val();
    var strLname =$('#lastname').val();
    var fnameLen = strFname.length;
    var lnameLen = strLname.length;
    if(fnameLen > 10){
        alert("ความยาวชื่อห้ามเกิน 50 ตัวอักษร") ;
        $('#firstname').val("")
        $('#firstname').focus();
        return false;
    }
    if(lnameLen > 10){
        alert("ความยาวนามสกุลห้ามเกิน 50 ตัวอักษร");
        $('#lastname').val("");
        $('#lastname').focus();
        return false;
    } 
    return false;

}

function validate() {
        var v_fname = $('#firstname').val();
        var v_lname = $('#lastname').val();
        var v_position = $('#position').val();
                if(v_fname== ""){
                    $('#afname').show();
                    $('#firstname').focus();
                }
                if(v_lname ==""){
                    $('#alname').show();
                    $('#lastname').focus();
                }
                if(v_position == "" ){
                    $('#aposition').show();
                    $('#position').focus();
                }
              return false;

}

function validate_hide() {
        var v_fname = $('#firstname').val();
        var v_lname = $('#lastname').val();
        var v_position = $('#position').val();
             if(v_fname != ""){
                    $('#afname').hide();
                }
                if(v_lname != ""){
                    $('#alname').hide();
                }
                if(v_position != ""){
                    $('#aposition').hide();
                }  
               return false;  

}

           
</script>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>